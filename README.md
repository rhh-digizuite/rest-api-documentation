Digizuite REST API
==================

### Table of contents
API conventions and endpoints.
See further specification below.

**General**

 * [Authentication](#authentication)
 * [Return values](#request-return-values)
 * [Errors](#errors)
 * [Request Query Language](#request-query-language)
 * [Request HTTP Headers](#request-http-headers)
 * [General data structure](#general-data-structure)

**Action endpoints**

 * [/reachability](#reachability)
 * [/import](#import)
 * [/export](#export)
 * [/request_reset_password](#reset-password)

**Model endpoints**

 * [/assets](#assets)
 * [/metadata_fields](#metadata_fields)

General
=======

### Authentication
In all calls that require authentication, the *user-agent* must pass its username and password, base64 encoded in an *Authentication* header.
See `http://en.wikipedia.org/wiki/Basic_access_authentication` for an example.

### Request return values
All calls to `model endpoints` return a JSON formatted representation of the requested, created or edited resource.
`DELETE` requests return `{success: true}`.
Return values for `custom endpoints` depends on the endpoint and are documented individually.

### Errors
Errors will return as a suiting HTTP status code and JSON data of the form  
```
{  
  errorCode: %http_error_code,  
  error: %error_message,  
  data: %additional_data_if_available  
}
```

### Request Query Language
All LIST request may include query parameters to make the result more specific.
Queries are applied as `GET-parameters`.

#### Pagination
Pagination can be applied directly through limit and offset parameters or together through the pagination parameter.

| Field      | Type        | Details  |
| :--------- | :---------- | :------- |
| limit      | Integer     | Maximum limit is 250 |
| offset     | Integer     | Default offset i 0, a limit *must* be provided when using offset |
| pagination | JSON Object | Send both as json object. Same rules apply. Example: {"limit": 10, "offset": 5}|

#### Conditions
Simple equality filters can be applied be prepending the property name with `filter-` and sending the key/value-pair as a GET parameter. I.e.:
```
/assets/?filter-public=true
```
More advanced filters can be applied through the `filters` parameter.  
The value should be a JSON array of filter objects, following the form specified below

| Field      | Required |   Type   | Details |
| -----      | :------: | :------: | ------  |
| property   |   YES    | `string` | This is the key you want to filter on. Keys can be nested with `>` or `.` operators |
| value      |   YES    | `string` | This is the value to search for |
| comparator |   NO     | `string` | The comparator of your condition. Default is `=`<br/>Allowed comparators are: `=`, `>`, `<`, `<=`, `>=`, `<=>`, `!=`, `<>`, `IS`, `IS NOT`, `LIKE`, `NOT LIKE`, `IN` |
| logical    |   NO     | `string` | Logical operator joining the condition on the set. Default is `AND`<br/>Allowed logicals are: `AND` ,`OR` , `XOR` |
| set        |   NO     | `string` | A set groups filter objects together. All filters without `set` explicitly defined will be grouped together |

**EXAMPLE**
```bash
/assets/?
  filters=[
    {'property':'public', 'value':'true'},
    {'property':'created_on', 'comparator':'>=', 'value':'1383042467', 'logical': 'OR', 'set' : 'date'},
    {'property':'created_on', 'comparator': 'IS',  'value':NULL, 'logical': 'OR', 'set' : 'date'}
  ]
```

#### Sorting
The resulting set can be sorted on all available properties.  
Sorting can be done on a single property by providing a string or on several by providing a JSON array.  

> **Tip:** For the simple sort, you can sort in the opposite direction by prepending `!` to the property name.  

**EXAMPLES**
```bash
  # Sort on created_on ASC
  /assets/?sort=created_on

  # Sort on timestamp DESC
  /assets/?sort=!created_on

  # Sort on public ASC, then created_on DESC
  /assets/?sort=[
    {'property':'public'},
    {'property':'created_on', 'direction':'DESC'}
  ]
```

General data structure
----------------------
> General structure of data is documented here.

### IDs
Unless otherwise specified, the model IDs are unique identifier strings (UUID)
You can always provide these IDs yourself when creating the model. If you do not one will be generated automatically.
The maximum length for IDs are **64 characters**.

---

Action endpoints
================

### Reachability
**GET /reachability**  
This call should always return 1 with status 200.
If this is not the case, something is wrong on the server.

### Import

> **NOTE:** THIS SHOULD BEHAVE ASYNCHRONOUSLY!!!!!

**POST /import**  
**POST /import/%FORMAT**  

> **NOTE:** Format is only relevant if a custom format (or mapper) has been provided for your task.
> Otherwise JSON will be used as default

Parameters:

| Field      | Required | Type | Details  |
| --------   | :---:    | :---:| ------- |
| file       | YES      | file | File containing data.<br/>Must follow `Digizuite JSON Format`  |
| behavior   | NO       |string| How the import is handled:<br/>INSERT_OR_UPDATE (default)<br/>EXCLUSIVE (delete everything else) |

### Export

> **NOTE:** THIS SHOULD BEHAVE ASYNCHRONOUSLY!!!!!

**GET /export**  
**GET /export/%FORMAT**  

*Default format is JSON*  
Export will return a file containing the requested data.

Parameters:

| Field      | Type | Default | Details  |
| --------   | :---:| :---:| ------- |
| models     | JSON array| *all* |Which models to be returned|
|created_after | timestamp |*0*||
|created_before| timestamp |*now*||
|updated_after | timestamp |*0*||
|updated_before| timestamp |*now*||


### Reset password
If a user forgets his or her password it can be changed using the reset password functionality.  

> **TODO:** DOCUMENT


Model endpoints
===============

### Assets

**GET /assets/**  
**GET /assets/%ID**  
**POST /assets/**  
**PATCH /assets/%ID**  
**DELETE /assets/%ID**  

| Field                | Required |     Type      | Details |
| -----                | :------: |     :---:     | ------  |
| id                   |    NO    |   `string`    |         |

### Metadata field types

**GET /metadata_field_types/**  
**GET /metadata_field_types/%ID**

| Field                     | Required |     Type      | Details |
| -----                     | :------: |     :---:     | ------  |
| id                        |    NO    |   `string`    |         |
| name                      |    YES   |   `string`    |         |
| description               |    YES   |   `string`    |         |

Default metadata field types as of this writing include:
> **TODO:** Write documentation for all metadata field types.

### Metadata fields

**GET /metadata_fields/**  
**GET /metadata_fields/%ID**  
**POST /metadata_fields/**  
**PATCH /metadata_fields/%ID**  
**DELETE /metadata_fields/%ID**  

| Field                     | Required |     Type      | Details |
| -----                     | :------: |     :---:     | ------  |
| id                        |    NO    |   `string`    |         |
| name                      |    YES   |   `string`    |         |
| metadata_field_type_id    |    YES   |   `string`    |         |